<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/category', name: 'app_category_')]
class CategoryController extends AbstractController
{
    #[Route('', name: "readall")]
    public function getAll(CategoryRepository $categRepo){
        $categories = $categRepo->findAll();

        return $this->render("category/list.html.twig", [
            "categories"=> $categories
        ]);
    }

    #[Route('/add', name: 'add')]
    public function add(EntityManagerInterface $em): Response
    {
        $category = new Category();
        $category->setNom("Back-End");
        $em->persist($category);
        $em->flush();

        return $this->render("category/add.html.twig", [
            "category"=> $category
        ]);
    }
}
