<?php

namespace App\Controller;

use App\Entity\Annonce;
use App\Repository\AnnonceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/annonce', name: 'app_annonce_')]
class AnnonceController extends AbstractController
{
    #[Route('', name: 'list')]
    public function index(AnnonceRepository $annonceRepository): Response
    {
        $annonces = $annonceRepository->findAll();

        return $this->render('annonce/list.html.twig', [
            'annonces' => $annonces
        ]);
    }

    #[Route('/add', name: 'add')]
    public function add(EntityManagerInterface $em): Response
    {
       $annonce = new Annonce();
       $annonce->setContent("Mon contenu");
       $annonce->setImage("https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Symfony_Welcome_Page.png/800px-Symfony_Welcome_Page.png");
       $annonce->setDateCreation(new \DateTime());

       $em->persist($annonce);
       $em->flush();

        return $this->render('annonce/success.html.twig');
    }

    #[Route('/{id}', name:'_detail')]
    public function detail(Annonce $annonce){
        return $this->render("annonce/detail.html.twig", [
            "annonce"=> $annonce
        ]);
    }

    #[Route('/remove/{id}', name:'_remove')]
    public function remove(Annonce $annonce, EntityManagerInterface $em){
        $em->remove($annonce);
        $em->flush();


        return new RedirectResponse($this->generateUrl("app_annonce_list"));
    }

    #[Route('/{id}/update', name:'_update')]
    public function update(Annonce $annonce, EntityManagerInterface $em){
        $annonce->setDateCreation(new \DateTime());
        $em->flush();

        return new RedirectResponse($this->generateUrl("app_annonce_list"));
    }
}
